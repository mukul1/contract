pragma solidity ^0.4.10;
import './SMTToken.sol';
import './Pausable.sol';
import './BTC.sol';
import './Utils.sol';
import './SafeMath.sol';
import './PricingStrategyPublic.sol';
import './Ownable.sol';
import './Sales.sol';

contract PublicICO is Ownable,Pausable, Utils,Sales{

    SMTToken token;
    uint256 public tokensPerUSD;
    uint256 public initialSupply;
    uint256 public currentSupply;
    PricingStrategyPublic pricingstrategy;

    uint256  public numberOfBackers;
    /* Max investment count when we are still allowed to change the multisig address */
    ///the txorigin is the web3.eth.coinbase account
    //record Transactions that have claimed ether to prevent the replay attacks
    //to-do
    mapping(uint256 => bool) transactionsClaimed;
    uint256 public valueToBeSent;
    uint public investorCount;

    ///the event log to log out the address of the multisig wallet
    event logaddr(address addr);

    //the constructor function
   function PublicICO(address tokenAddress,address strategy){
        //require(bytes(_name).length > 0 && bytes(_symbol).length > 0); // validate input
        token = SMTToken(tokenAddress);
        tokensPerEther = token.tokensPerEther();
        tokensPerBTC = token.tokensPerBTC();
        valueToBeSent = token.valueToBeSent();
        pricingstrategy = PricingStrategyPublic(strategy);
    }

    /**
        Payable function to send the ether funds
    **/
    event logstate(ICOSaleState state);
    ////Here we will put up our payable function
    ////for the ethers 
    
    function tokenAssignExchange(address addr,uint256 value) external onlyOwner returns(bool){
         bool  isValid = token.isValid();
        if(!isValid) throw;
        if (value == 0) throw;
         ICOSaleState currentState = getStateFunding();
        logstate(currentState);
        if(currentState==ICOSaleState.Failed) throw;
        if(initialSupply>token.tokenCreationMax()) throw;
         var (discount,usd) = pricingstrategy.totalDiscount(currentState,value,"ethereum");
        uint256 tokens = usd*tokensPerUSD;
        uint256 totalTokens = SafeMath.add(tokens,SafeMath.div(SafeMath.mul(tokens,discount),100));
        initialSupply = SafeMath.add(initialSupply,totalTokens);
        tokenCreationMax = SafeMath.sub(tokenCreationMax,totalTokens);
        token.addToBalances(addr,totalTokens);
        token.increaseEthRaised(value);
        numberOfBackers++;
        token.increaseUSDRaised(usd);
        return true;
    }

    //Token distribution for the case of the ICO
    ///function to run when the transaction has been veified
    function processTransaction(bytes txn, uint256 txHash,address addr,bytes20 btcaddr) onlyOwner returns (uint)
    {
        bool  valueSent;
        bool  isValid = token.isValid();
        if(!isValid) throw;
     ICOSaleState currentState = getStateFunding();

        if(!transactionsClaimed[txHash]){
            var (a,b) = BTC.checkValueSent(txn,btcaddr,valueToBeSent);
            if(a){
                valueSent = true;
                transactionsClaimed[txHash] = true;
                 ///since we are creating tokens we need to increase the total supply
               allottTokensBTC(addr,b,currentState);
        return 1;
        }
            }

    }
    
    ///function to allot tokens to address
    function allottTokensBTC(address addr,uint256 value,ICOSaleState state) internal{
        ICOSaleState currentState = getStateFunding();

        if(currentState==ICOSaleState.Failed) throw;

        if(initialSupply>token.tokenCreationMax()) throw;
        var (discount,usd) = pricingstrategy.totalDiscount(state,value,"bitcoin");

        uint256 tokens = usd*tokensPerUSD;
        uint256 totalTokens = SafeMath.add(tokens,SafeMath.div(SafeMath.mul(tokens,discount),100));
        initialSupply = SafeMath.add(initialSupply,totalTokens);
        tokenCreationMax = SafeMath.sub(tokenCreationMax,totalTokens);
        token.addToBalances(addr,totalTokens);
        numberOfBackers++;
        token.increaseBTCRaised(value);
        token.increaseUSDRaised(usd);
    }

    function finalizeTokenSale() public onlyOwner{
        ICOSaleState currentState = getStateFunding();
        token.finalizeICO();
    }


    

}