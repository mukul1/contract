require('babel-polyfill');
var PreICO = artifacts.require('PreICO');
var 
const InvalidAddress = '0x0';
/**
Public ICO test cases for the SMT Token
**/

contract('PreICO',function(accounts){
	/**
	Test that checks whether contract displays the corrct name of the token
	**/
	it("should correctly display the name", async() => {
		let preico = await PreICO.new(1,1000);
		let name = await preico.name.call();
		assert.equal(name,"Sun Money Token");
	});

	/**
	Test that checks whether contract displays the corrct symbol of the token
	**/
	it("should correctly display the symbol", async() => {
		let preico = await PreICO.new(1,1000);
		let symbol = await preico.symbol.call();
		assert.equal(symbol,"SMT");
	});

	/**
	Test that checks whether contract displays the corrct decimals of the token
	**/
	it("should correctly display the decimals", async() => {
		let preico = await PreICO.new(1,1000);
		let decimals = await preico.decimals.call();
		assert.equal(decimals,18);
	});

	/**
	Test that checks whether contract displays the corrct version of the token
	**/
	it("should correctly display the version", async() => {
		let preico = await PreICO.new(1,1000);
		let version = await preico.version.call();
		assert.equal(version,"1.0");
	});

	/**
	Test that checks whether contract displays the corrct endblock of the token
	**/
	it('verifies the funding endBlock has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let endBlock = await preico.fundingEndBlock.call();
		assert.equal(endBlock,1000);
	});

	/**
	Test that checks whether contract displays the corrct startblock of the token
	**/
	it('verifies the funding startBlock has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let startBlock = await preico.fundingStartBlock.call();
		assert.equal(startBlock,1);
	});

	/**
	Test that checks whether contract displays the corrct startblock of the token
	**/
	it('verifies the funding SMTfund has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let smtfund = await preico.SMTfund.call();
		assert.equal(smtfund,10 * (10**6) * 10**18);
	});

	/**
	Test that checks whether contract displays the corrct finalizedPreICO of the token
	**/
	it('verifies the finalizedPreICO has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let finalized = await preico.finalizedPreICO.call();
		assert.equal(finalized,false);
	});

	/**
	Test that checks whether contract displays the corrct tokensperether of the token
	**/
	it('verifies the funding tokensperether has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let tke = await preico.tokensPerEther.call();
		assert.equal(tke,4400);
	});

	/**
	Test that checks whether contract displays the corrct tokensperbtc of the token
	**/
	it('verifies the funding tokensperbtc has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let tpb = await preico.tokensPerBTC.call();
		assert.equal(tpb,700000000);
	});


	/**
	Test that checks whether contract displays the corrct initialSupplyPreICO of the token
	**/
	it('verifies the funding initialSupplyPreICO has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let preicosupply = await preico.initialSupplyPreICO.call();
		assert.equal(preicosupply,0);
	});

	/**
	Test that checks whether contract displays the corrct initialSupplyPrivateSale of the token
	**/
	it('verifies the funding initialSupplyPrivateSale has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let privatesale = await preico.initialSupplyPrivateSale.call();
		assert.equal(privatesale,0);
	});

	/**
	Test that checks whether contract displays the corrct initialSupplyPublicPreICO of the token
	**/
	it('verifies the funding initialSupplyPublicPreICO has been set',async()=>{
		let preico = await PreICO.new(1,1000);
		let publicsale = await preico.initialSupplyPublicPreICO.call();
		assert.equal(publicsale,0);
	});

	/**Test the payment thing in the preico contract **/
	it('verifies that the ethereum payments are working fine in PreICO contract', async() => {
		let preico = await PreICO.new(1,1000);
		let initialBalanceOwner = await web3.eth.getBalance(web3.eth.coinbase);
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:preico.address,value:10000,gasLimit:1000000,gas:300000});
		let tokensRecieved = await preico.balanceOf.call(web3.eth.accounts[5]);
		//check the balance of the  owner
		let balanceOfOwner = await web3.eth.getBalance(web3.eth.coinbase);
		let balanceChange = balanceOfOwner-initialBalanceOwner;
		let supply = await preico.initialSupplyPrivateSale.call();
		///balance should be increased
		assert.equal(balanceChange,8192);
		///tokens should have been recieved
		assert.equal(tokensRecieved.toNumber(),10000);
		///supply should be increased by this amount
		assert.equal(supply,tokensRecieved);
	});	

	/**Test that the contract is working fine with the ethereum exchange**/
	/** that are we able to push the transactions 
	**/

	it('verifies that owner can push to the ethereum exchange', async() => {
		let preico = await PreICO.new(1,1000);
		let balanceOfOwner = await web3.eth.getBalance(web3.eth.coinbase);
		await preico.tokenAssignExchange(web3.eth.accounts[2],1*10**17);
		let tokensRecieved = await preico.balanceOf.call(web3.eth.accounts[2]);
		assert.equal(tokensRecieved.toNumber(),1*10**17);
		let supply = await preico.initialSupplyPrivateSale.call();
		assert.equal(supply,tokensRecieved);

	});

	/**
	Other one cant push other than owner
	**/
	it('verifies any other person cant push other than the owner', async()=> {
		let preico = await PreICO.new({from:web3.eth.accounts[5],gasLimit:10000000,gas:4500000});
		let balanceOfOwner = await web3.eth.getBalance(web3.eth.coinbase);
		try{
			await preico.tokenAssignExchange(web3.eth.accounts[2],1*10**17);
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
		//should be throwing an error
	});

	///but the same should be working with the owner
	it('verifies any other person cant push other than the owner but owner can', async()=> {
		let preico = await PreICO.new();
		let balanceOfOwner = await web3.eth.getBalance(web3.eth.coinbase);
		await preico.tokenAssignExchange(web3.eth.accounts[2],1*10**17);
		//should not be throwing an error now
	});


})