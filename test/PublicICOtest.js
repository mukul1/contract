require('babel-polyfill');
var PublicICO = artifacts.require('PublicICO');
const InvalidAddress = '0x0';
/**
Public ICO test cases for the SMT Token
**/

contract('PublicICO',function(accounts){
	it("should correctly display the funds", async() => {
		let publicico = await PublicICO.new(1,1000,"0xdcb7fe57e84551cf0ad35addee84351ed32454d3");
		let fund = await publicico.tokenCreationMaxPublicICO.call();
		console.log(fund);
	});
})