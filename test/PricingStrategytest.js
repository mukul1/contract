require('babel-polyfill');
var PricingStrategy = artifacts.require('PricingStrategy');
const InvalidAddress = '0x0';
/**
Testing of the pricing strategy contract fo the SunMoney ICO
**/
contract('PricingStrategy',function(accounts){
	//////////BASE DISCOUNT/////////////////////////////////////////////
	///Base Discount when current supply is between 1 and 1.5 million
	///contribution > 3 ETHERS
	//Should return 40
	it("should output correct Base Discount when supply between 1 and 1.5 mill and contrib > 3", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.baseDiscounts.call(1*10**6,4*10**18);
		assert.equal(baseDiscount.toNumber(),40);
	});
	///Base Discount when current supply is between 1.5million and 3 million
	///contribution > 1 ETHERS
	//Should return 30
	it("should output correct Base Discount when supply between 1.5mill and 3 mill and contrib > 1", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.baseDiscounts.call(2*10**6,2*10**18);
		assert.equal(baseDiscount.toNumber(),30);
	});
	///Base Discount when current supply is between 1 and 1.5 million
	///contribution = 3 ETHERS
	//Should return 0
	it("should output correct Base Discount when supply between 1 and 1.5 mill and contrib = 3", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.baseDiscounts.call(1*10**6,3*10**18);
		assert.equal(baseDiscount.toNumber(),0);
	});
	///Base Discount when current supply is between 1.5 million and 3 million
	///contribution = 1 ETHERS
	//Should return 0
	it("should output correct Base Discount when supply between 1 and 1.5 mill and contrib = 1", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.baseDiscounts.call(2*10**6,1*10**18);
		assert.equal(baseDiscount.toNumber(),0);
	});
	///Base Discount when current supply is between 1.5 million and 3 million
	///contribution = 0.5 ETHERS
	//Should return 0
	it("should output correct Base Discount when supply between 1 and 1.5 mill and contrib = 0.5", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.baseDiscounts.call(2*10**6,0.5*10**18);
		assert.equal(baseDiscount.toNumber(),0);
	});
	///Base Discounts should fail at 0 current Supply 
	it("should fail at zero current Supply", async() =>{
		let pricingstrategy = await PricingStrategy.new();
		try{
			await pricingstrategy.baseDiscounts.call(0,1*10**18);
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});
	///Base Discounts should fail at 0 contribution
	it("should fail at zero current contribution", async() =>{
		let pricingstrategy = await PricingStrategy.new();
		try{
			await pricingstrategy.baseDiscounts.call(1110,0);
			assert(false,'didnt throw');
		}catch(error){
			return error;
		}
	});

	/////////////VOLUME DISCOUNT//////////////////////
	/**
	Should return the volume based discounts correctly
	**/

	///contribution > 3 ETHERS <10 ETHERS
	//Should return 0
	it("should output correct volume Discount when  contrib > 3 and <10", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.volumeDiscounts.call(4*10**18);
		assert.equal(baseDiscount.toNumber(),0);
	});
	///contribution >=10 and <20
	//Should return 5
	it("should output correct volume Discount when  contrib > 3 and <10", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.volumeDiscounts.call(14*10**18);
		assert.equal(baseDiscount.toNumber(),5);
	});
	///contribution >20
	//Should return 10
	it("should output correct volume Discount when  contrib > 3 and <10", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.volumeDiscounts.call(22*10**18);
		assert.equal(baseDiscount.toNumber(),10);
	});
	///contribution 2
	//Should return 0
	it("should output correct volume Discount when  contrib > 3 and <10", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.volumeDiscounts.call(2*10**18);
		assert.equal(baseDiscount.toNumber(),0);
	});

	/////////////TOTAL DISCOUNT//////////////////////
	/**
		get the total discount
	**/

	///should show the total discounts correctly
	it("should output correct total discount when case1", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.totalDiscount.call(2*10**6,0.5*10**18);
		assert.equal(baseDiscount.toNumber(),0);
	});

	it("should output correct total discount when case2", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.totalDiscount.call(4*10**18,0.5*10**1);
		assert.equal(baseDiscount.toNumber(),0);
	});

	it("should output correct total discount when case3", async() => {
		let pricingstrategy = await PricingStrategy.new();
		let baseDiscount = await pricingstrategy.totalDiscount.call(4*10**18,0.5*10**1);
		assert.equal(baseDiscount.toNumber(),0);
	});


});