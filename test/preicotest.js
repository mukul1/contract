require('babel-polyfill');
var PreICO = artifacts.require('PreICO');
var PublicICO = artifacts.require('PublicICO');
var SMTToken = artifacts.require('SMTToken');
const InvalidAddress = '0x0';

contract('SMTToken',function(accounts){
	it("shuld perform as expected full end to end testing",async() =>{
		///first deplo the smt token
		let smttoken = await SMTToken.new(1,1000);
		let addressSMT = smttoken.address;
		let preico = await PreICO.new(addressSMT);
		///deply the public ico contract also
		let publicico = await PublicICO.new(addressSMT);
		///just for documentation purposes
		console.log(addressSMT);
		///Get the owner and should be equalt to the web3.eth.coinbase
		let owners = await smttoken.owner.call();
		assert.equal(owners,web3.eth.coinbase);
		///should not allow writing to balances array of smt token unless added to valid owners array
		try{
			await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:preico.address,value:100,gasLimit:1000000,gas:300000});	
			assert(false,'didnt throw');
		}catch(error){
			// console.log(error) ;
		}
		///should not allow tokenassignexxchange to run unless added to valid owner array
		try{
			await preico.tokenAssignExchange(web3.eth.accounts[6],100,{from:web3.eth.coinbase,gasLimit:1000000,gas:300000});
			assert(false,'didnt throw');
		}catch(error){
			// console.log(error) ;
		}
		///should not be able to send from the public ico contract as welll
		try{
			await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:publicico.address,value:100,gasLimit:1000000,gas:300000});	
			assert(false,'didnt throw');
		}catch(error){
			// console.log(error) ;
		}
		///should not allow tokenassignexxchange to run unless added to valid owner array
		try{
			await publicico.tokenAssignExchange(web3.eth.accounts[6],100,{from:web3.eth.coinbase,gasLimit:1000000,gas:300000});
			assert(false,'didnt throw');
		}catch(error){
			// console.log(error) ;
		}
		///then add the PreICO as the owner
		await smttoken.addToOwnership(preico.address);
		///it should have been added
		///check the initial state should be qual to zero()
		let currentState = await smttoken.salestate.call();
		assert.equal(currentState,0);
		///as we have the correct state check all balances are initially zero
		let balancesmt = await web3.eth.getBalance(smttoken.address);
		let balancepreico = await web3.eth.getBalance(preico.address);
		assert.equal(balancesmt,0);
		assert.equal(balancepreico,0);
		///since this is true we will check sending money to the preico contract
		///and check for the additions in the smttoken balance of values
		let valueToSend = 100;
		await web3.eth.sendTransaction({from:web3.eth.accounts[5],to:preico.address,value:valueToSend,gasLimit:1000000,gas:300000});
		///above hits the payable function
		let valueBal = await smttoken.balanceOf.call(web3.eth.accounts[5]);
		let etherExchangeRate = await smttoken.tokensPerEther.call();
		// console.log(valueBal);
		assert.equal(valueBal,valueToSend*etherExchangeRate);
		///check weather the discounting is working or not
		//try sennding from a new address a ammount that deserver the discounting
		///see discounting price trance for more details
		await web3.eth.sendTransaction({from:web3.eth.accounts[3],to:preico.address,value:4*10,gasLimit:1000000,gas:300000});
		let balancesOFval = await smttoken.balanceOf.call(web3.eth.accounts[3]);
		assert.equal(balancesOFval.toNumber(),4400*(4*10));
		///check whether the supply is increasing or not
		let supplyPreICOval = await preico.initialSupplyPreSale.call();
		///for the preico it is zero bcause the state is private pre sale
		assert.equal(0,supplyPreICOval);
		let supplyPreSalePrivate = await preico.initialSupplyPrivateSale.call();
		assert.equal(440000,supplyPreSalePrivate.toNumber());
		///since this is true we 	check the tokenAssignExchangestuff
		//for ether exchanges
		let valueToSendForExchanges = 110;
		await preico.tokenAssignExchange(web3.eth.accounts[6],valueToSendForExchanges,{from:web3.eth.coinbase,gasLimit:1000000,gas:300000});
		///the balances should hve been increased
		valueBal = await smttoken.balanceOf.call(web3.eth.accounts[6]);
		assert.equal(valueBal.toNumber(),valueToSendForExchanges*etherExchangeRate);
		///should not allow web3.account[5 and 6] to transfer tokens
		// assert.equalt(0,100);
		///check weather the values are being transferred to the owner
		let initalBalanceOwner = await web3.eth.getBalance(web3.eth.coinbase);
		console.log(initalBalanceOwner);
		await web3.eth.sendTransaction({from:web3.eth.accounts[4],to:preico.address,value:1000,gasLimit:1000000,gas:300000});
		let finalBalanceOwner = await web3.eth.getBalance(web3.eth.coinbase);
		///again console log for documentation purposes
		console.log(finalBalanceOwner);
		///assert equal
		assert.equal(10000000000000,(finalBalanceOwner-initalBalanceOwner));
		///Add PublicICO to the list of owners and
		///do the same i.e. test that balancesOf can be added
		///via public ICO
		await smttoken.addToOwnership(publicico.address);
		///Value to send in the public ico
		let valueToSendpublicico = 100;
		await web3.eth.sendTransaction({from:web3.eth.accounts[2],to:preico.address,value:valueToSendpublicico,gasLimit:1000000,gas:300000});
		///above hits the payable function
		let valueBal = await smttoken.balanceOf.call(web3.eth.accounts[2]);
		let etherExchangeRate = await smttoken.tokensPerEther.call();
		// console.log(valueBal);
		assert.equal(valueBal,valueToSend*etherExchangeRate);
		///Hits the tokenAssignExchange function
		///Hits the function
		let valueToSendForExchangespublicico = 110;
		await preico.tokenAssignExchange(web3.eth.accounts[3],valueToSendForExchangespublicico,{from:web3.eth.coinbase,gasLimit:1000000,gas:300000});
		///the balances should hve been increased
		valueBal = await smttoken.balanceOf.call(web3.eth.accounts[3]);
		assert.equal(valueBal.toNumber(),valueToSendForExchangespublicico*etherExchangeRate);
		///now lets try to change the state
		await smttoken.setState(2);
		///now get the state
		let newStateChange = smttoken.currentState.call();
		assert.equal(newStateChange,2);
		///once state is changed then we will proceed
		///follows
		

	});

})